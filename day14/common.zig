const std = @import("std");

pub const Row = std.ArrayList(u8);
pub const Map = std.ArrayList(Row);
pub const all = std.heap.page_allocator;

// Converts maps to linear string to save it in a Hash Map
pub fn mapToStr(map: Map) !Row {
    var out = Row.init(all);
    for (map.items) |row| {
        try out.appendSlice(row.items);
    }
    return out;
}

// Transposes matrix
pub fn transpose(pattern: Map) !Map {
    var out = Map.init(all);
    const w = pattern.getLast().items.len;
    for (0..w) |col| {
        var prep = Row.init(all);
        for (pattern.items) |row| {
            try prep.append(row.items[col]);
        }
        try out.append(prep);
    }
    return out;
}

// Mirrors Matrix Vertically
pub fn mirrorVertical(pattern: Map) !Map {
    var out = try pattern.clone();
    for (out.items) |*row| {
        std.mem.reverse(u8, row.*.items);
    }
    return out;
}

// Mirrors Matrix Horizontally
pub fn mirrorHorizontal(pattern: Map) !Map {
    var out = try pattern.clone();
    std.mem.reverse(Row, out.items);
    return out;
}

// Appends a certain number of copies of an element to a row
fn appendN(row: *Row, symbol: u8, count: usize) !void {
    for (0..count) |_| {
        try row.*.append(symbol);
    }
}

// Compacts (e.g. tilts) a single row, to the left
fn compact(row: Row) !Row {
    var countrocks: usize = 0; // Number of rocks in segment
    var countempty: usize = 0; // Number of empty spaces in segment
    var outrow = Row.init(all);
    for (row.items) |c| {
        if (c == '#') { // Found a fixed rock, segment ends
            try appendN(&outrow, 'O', countrocks); // Adds all the round rocks
            try appendN(&outrow, '.', countempty); // Adds the spaces
            try outrow.append('#'); // Adds this rock
            countrocks = 0; // Reset
            countempty = 0;
        }
        if (c == 'O') {
            countrocks += 1;
        }
        if (c == '.') {
            countempty += 1;
        }
    }
    try appendN(&outrow, 'O', countrocks); // Leftovers
    try appendN(&outrow, '.', countempty);
    return outrow;
}

// Roll the entire map to the west (natural compact order)
pub fn rollWest(map: Map) !Map {
    var work = try map.clone();
    for (work.items) |*row| {
        row.* = try compact(row.*);
    }
    return work;
}

// Rolls east by mirroring
pub fn rollEast(map: Map) !Map {
    return try mirrorVertical(try rollWest(try mirrorVertical(map)));
}

// Rolls north by transposing
pub fn rollNorth(map: Map) !Map {
    return try transpose(try rollWest(try transpose(map)));
}

// Rolls south by transposing and mirroring
pub fn rollSouth(map: Map) !Map {
    return try mirrorHorizontal(try rollNorth(try mirrorHorizontal(map)));
}

// Find the load as per problem
pub fn calcLoad(map: Map) usize {
    var load: usize = 0;
    for (0.., map.items) |i, row| {
        // Factor decreases from top row to 1
        var val = map.items.len - i;
        // Number of rocks
        var count = std.mem.count(u8, row.items, "O");
        load += count * val;
    }
    return load;
}
