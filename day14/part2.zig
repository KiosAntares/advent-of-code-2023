const std = @import("std");
const cm = @import("common.zig");

// One single cycle.
// Note: could be optimised by rotating and using West instead
// of using all the mirroring and transposing single functions
fn cycle(map: cm.Map) !cm.Map {
    var out = try cm.rollNorth(map);
    out = try cm.rollWest(out);
    out = try cm.rollSouth(out);
    out = try cm.rollEast(out);
    return out;
}

// Executes Target number of cycles.
// To reduce work, we save visited configurations, with the number
// of cycles that took to get to it.
// As soon as we encounter one that is already saved,
// We can know skip any multiple of that number of cycles from the target
// and just run the remainder at the end.
// This way, we only need 2 * repetition_cycles - 1 cycles at most
fn runCycles(imap: cm.Map, target: usize) !cm.Map {
    // All the visited states. We can't hash the map directly,
    // so we use a string representation of it
    var visited = std.StringHashMap(usize).init(cm.all);
    // Work map to iterate over
    var map = try imap.clone();
    // String representation of the current map
    var mapStr = (try cm.mapToStr(map)).items;
    // Number of cycles to get to this configuration
    var count: usize = 0;
    // Until found or we just end
    while (!visited.contains(mapStr) and count < target) {
        // Save as visited
        try visited.put(mapStr, count);
        // Calculate new state and relative string representation
        map = try cycle(map);
        mapStr = (try cm.mapToStr(map)).items;
        count += 1;
    }

    // Get latest state
    if (visited.get(mapStr)) |last| {
        // Get how many cycles we need to do at the end after we skip
        // the multiple of count
        var remainder = (target - count) % (count - last);
        // Run those cycles
        for (0..remainder) |_| {
            map = try cycle(map);
        }
    }
    return map;
}

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
    var buf: [1024]u8 = undefined;

    var map = cm.Map.init(cm.all);
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        var row = cm.Row.init(cm.all);
        try row.appendSlice(line);
        try map.append(row);
    }

    map = try runCycles(map, 1000000000);
    var load = cm.calcLoad(map);

    try stdout.print("{d} \n", .{load});
}
