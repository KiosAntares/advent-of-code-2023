const std = @import("std");
const cm = @import("common.zig");

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
    var buf: [1024]u8 = undefined;

    var patterns = std.ArrayList(cm.Map).init(cm.all);
    var pattern = cm.Map{ .width = undefined, .rows = cm.Map.Rows.init(cm.all) };
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        // On an empty line there is the separation between two patterns
        if (line.len == 0) {
            // so we save the pattern
            try patterns.append(pattern);
            // start on a new one
            pattern = cm.Map{ .width = undefined, .rows = cm.Map.Rows.init(cm.all) };
            // and skip the empty line
            continue;
        }

        // Construct the new binary row
        var row: u64 = 0;
        for (line) |c| {
            var bit: u1 = 0;
            if (c == '#') { // We use 1 for the mirror
                bit = 1;
            }
            row <<= 1; // Give one new space
            row |= bit; // Then set the bit
        }
        pattern.width = line.len; // Set the width of the binary data
        try pattern.rows.append(row); // And add the row
    }
    try patterns.append(pattern); // Add the last pattern we worked on, as
    // there wasn't an empty line to save

    var cols: usize = 0;
    var rows: usize = 0;
    for (patterns.items) |p| {
        const row = cm.findSymmetry(p, 1);
        const col = cm.findSymmetry(try cm.transpose(p), 1);
        if (col.size == 0) {
            rows += row.index;
        } else {
            cols += col.index;
        }
    }

    try stdout.print("{d} \n", .{rows * 100 + cols});
}
