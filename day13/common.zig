const std = @import("std");

pub const Row = u64;
pub const all = std.heap.page_allocator;

pub const Map = struct {
    pub const Rows = std.ArrayList(Row);
    width: usize,
    rows: Rows,
};

// Data about a symmetry
pub const Symmetry = struct {
    index: usize,
    size: usize,
};

// Transposes a binary matrix, right to left
pub fn transpose(pattern: Map) !Map {
    var out = Map{
        .width = pattern.rows.items.len,
        .rows = Map.Rows.init(all),
    };
    const w = pattern.width;
    for (0..w) |col| {
        var prep: u64 = 0;
        for (pattern.rows.items) |row| {
            // Select col-th bit from the right and add it to the preparation line
            prep |= (row >> @truncate(col)) & 1;
            // Move to new column in preparation line
            prep <<= 1;
        }
        try out.rows.insert(0, prep >> 1); // We rotate once too many times
    }
    return out;
}

pub fn findSymmetry(pattern: Map, smudges: usize) Symmetry {
    const w = pattern.rows.items.len;
    const rows = pattern.rows.items;
    // Select rows
    for (1..w) |r| {
        // Find maximum possible size of the symmetry on this row
        const symw = @min(r, w - r);
        var size: usize = 0;
        var diffs: usize = 0;
        // While we still have space and we haven't consumed too many smudges
        while (size < symw and diffs <= smudges) {
            // By using the xor we can find the differences between the two rows
            // The pop count gives the number of differences, aka set bits.
            diffs += @popCount(rows[r - size - 1] ^ rows[r + size]);
            size += 1;
        }
        // If we used all possible space with the right number of differences
        // then we found the symmetry
        if (size == symw and diffs == smudges) {
            return Symmetry{ .index = r, .size = size };
        }
    }
    // If we didn't find anything then there's no symmetry
    return Symmetry{ .index = 0, .size = 0 };
}
