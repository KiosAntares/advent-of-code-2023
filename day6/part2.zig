const std = @import("std");
const util = @import("lib/utils.zig");
const common = @import("common.zig");

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input2", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
    var buf: [1024]u8 = undefined;

    // to parse these, filter out anything not digit (numbers are all positive)
    // only use one arraylist to decrease operations
    var time: u64 = undefined;
    var cleanline = std.ArrayList(u8).init(std.heap.page_allocator);
    if (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        for (line) |c| {
            if (std.ascii.isDigit(c))
                try cleanline.append(c);
        }
        std.debug.print("TIME: {s}\n", .{cleanline.items});
        time = try std.fmt.parseInt(u64, cleanline.items, 10);
    }
    // We clear retaining capacity, hoping that the new number fits
    cleanline.clearRetainingCapacity();
    var distance: u64 = undefined;
    if (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        for (line) |c| {
            if (std.ascii.isDigit(c))
                try cleanline.append(c);
        }
        std.debug.print("DISTANCE: {s}\n", .{cleanline.items});
        distance = try std.fmt.parseInt(u64, cleanline.items, 10);
    }

    // Testing if mathematically solving works
    var s = common.solve(time, distance);
    var ss = s.high - s.low;
    var sim = common.simulate(time, distance);
    if (ss == sim) {
        std.debug.print("SOLVE PASSED\n", .{});
    }
    try stdout.print("{d} ({d})\n", .{ sim, ss });
}
