const std = @import("std");

pub const Range = struct {
    high: u64,
    low: u64,
};
//
// NOTE: the solving appears to work on any actual input file provided, it does
// however fail on the third example case from the page. It's therefore not used
// to calculate the final result but it's still displayed and kept.
//
pub fn solve(t: u64, d: u64) Range {
    const D: f64 = @floatFromInt(d);
    const T: f64 = @floatFromInt(t);
    const inside: f64 = T * T - 4.0 * D;
    const rh = std.math.sqrt(inside);
    return Range{
        .low = @intFromFloat((T - rh) / 2),
        .high = @intFromFloat((T + rh) / 2),
    };
}

pub fn simulate(t: u64, d: u64) u32 {
    var c: u32 = 0;
    var M: u32 = 0;
    for (0..t) |b| {
        var m = b * (t - b);
        // only simulate until you find the maximum
        if (m < M) {
            return c;
        }
        if (m > d) {
            c += 1;
            m = M;
        }
    }
    return c;
}
