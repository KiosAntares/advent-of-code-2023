const std = @import("std");
const util = @import("lib/utils.zig");
const common = @import("common.zig");

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input2", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
    var buf: [1024]u8 = undefined;

    var times: std.ArrayList(u64) = undefined;
    if (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        times = try util.parseSeparatedList(u64, line[5..], " ");
    }

    var distances: std.ArrayList(u64) = undefined;
    if (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        distances = try util.parseSeparatedList(u64, line[9..], " ");
    }

    var total: u32 = 1;
    for (times.items, distances.items) |t, d| {
        var sim = common.simulate(t, d);
        var sol = common.solve(t, d);
        std.debug.print("{d}, {d} -> {d} ({d})\n", .{ t, d, sim, sol.high - sol.low });
        total *= sim;
    }

    try stdout.print("{d}\n", .{total});
}
