const std = @import("std");
const util = @import("lib/utils.zig");
const cm = @import("common.zig");

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
    var buf: [1024]u8 = undefined;

    var total: i32 = 0;
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        var s = try util.parseSeparatedList(i32, line, " ");
        var sub = try cm.makeSequence(s);
        // for (sub.set.items) |diff| {
        //     for (diff.items) |e| {
        //         std.debug.print("{d} ", .{e});
        //     }
        //     std.debug.print("\n", .{});
        // }
        var wb = try cm.walkBackFront(sub);
        // for (wb.items) |e| {
        //     std.debug.print("{d} ", .{e});
        // }
        total += wb.items[0];
        // std.debug.print("\n\n", .{});
    }

    try stdout.print("{d} \n", .{total});
}
