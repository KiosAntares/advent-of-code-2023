const std = @import("std");
const util = @import("lib/utils.zig");

pub const Seq = std.ArrayList(i32);
pub const all = std.heap.page_allocator;

pub const Sequence = struct {
    set: std.ArrayList(Seq),
};

// Makes a sequence containing the difference between elements of a sequence,
// left to right
fn makeDifference(seq: Seq) !Seq {
    var s = Seq.init(all);
    for (0..seq.items.len - 1) |i| {
        try s.append(seq.items[i + 1] - seq.items[i]);
    }
    return s;
}

// Checks if a sequence only contains 0
fn allZero(seq: Seq) bool {
    for (seq.items) |item| {
        if (item != 0) {
            return false;
        }
    }
    return true;
}

// Constructs all sequences of the stack
pub fn makeSequence(seq: Seq) !Sequence {
    var out = Sequence{
        .set = std.ArrayList(Seq).init(all),
    };
    // We start by adding the original one
    try out.set.append(seq);
    // And compute the initial difference
    var q = try makeDifference(seq);
    while (!allZero(q)) { // Until there is no change,
        // Add (independently allocated) sequence
        try out.set.append(try q.clone());
        // Compute new difference
        q = try makeDifference(q);
    }
    return out;
}

// Walking bottom-up the stack of differences, extrapolate the next element of
// every layer of the stack. To do this, we take the last element of the nth + 1 layer
// and add it to the last element of the nth layer.
pub fn walkBack(sequ: Sequence) !Seq {
    var i: usize = sequ.set.items.len;
    // Get bottom sequence and add a copy of its last element to the end.
    // When we get here, the subsequent difference would be 0s, so all elements
    // are the same
    try sequ.set.items[i - 1].append(sequ.set.getLast().getLast());
    // Now we start extrapolating (starting at second-to-last)
    i -= 1;
    while (i > 0) {
        i -= 1;
        // add to nth layer the last of nth + 1 summed to the last of nth
        try sequ.set.items[i].append(sequ.set.items[i].getLast() + sequ.set.items[i + 1].getLast());
    }
    // Return a copy
    return sequ.set.items[0].clone();
}

// Same as above, but for the front of the sequence (extrapolate backwards)
pub fn walkBackFront(sequ: Sequence) !Seq {
    var i: usize = sequ.set.items.len;
    // Insert a copy of the first element at the front
    try sequ.set.items[i - 1].insert(0, sequ.set.getLast().items[0]);
    i -= 1;
    while (i > 0) {
        i -= 1;
        // We do the same thing but subtracting and inserting at the front
        try sequ.set.items[i].insert(0, sequ.set.items[i].items[0] - sequ.set.items[i + 1].items[0]);
    }
    return sequ.set.items[0].clone();
}
