const std = @import("std");
const util = @import("lib/utils.zig");

// Hash function as described by problem
fn hash(str: []const u8) u8 {
    var out: u32 = 0;
    for (str) |c| {
        out += c;
        out *= 17;
        out %= 256;
    }
    return @truncate(out);
}

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
    var buf: [25000]u8 = undefined; // Line is long

    var s = std.ArrayList(u8).init(std.heap.page_allocator);
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        try s.appendSlice(line); // deal with any undesired end of lines
    }

    var tokens = try util.splitOn(s.items, ","); // Tokenize on ','
    var total: u32 = 0;
    for (tokens.items) |token| {
        total += hash(token);
    }

    try stdout.print("{d} \n", .{total});
}
