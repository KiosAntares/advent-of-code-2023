const std = @import("std");
const util = @import("lib/utils.zig");

// Hash function as described by problem
fn hash(str: []const u8) u8 {
    var out: u32 = 0;
    for (str) |c| {
        out += c;
        out *= 17;
        out %= 256;
    }
    return @truncate(out);
}

// A single box. Uses ArrayHashMap to maintain order
const Box = std.StringArrayHashMap(u8);

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
    var buf: [25000]u8 = undefined;

    var s = std.ArrayList(u8).init(std.heap.page_allocator);
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        try s.appendSlice(line);
    }

    var tokens = try util.splitOn(s.items, ",");

    // Initialize all boxes
    var boxes: [256]Box = undefined;
    for (&boxes) |*box| {
        box.* = Box.init(std.heap.page_allocator);
    }

    for (tokens.items) |token| {
        // Always split token on =. For -, we just assume if it wasn't split
        var pieces = try util.splitOn(token, "=");
        // It wasn't split (1) piece, so it's -
        if (pieces.items.len == 1) {
            var label = pieces.items[0];
            label = label[0 .. label.len - 1]; // Get label (everything but the -)
            _ = boxes[hash(label)].orderedRemove(label); // Remove that from the box
        } else {
            var label = pieces.items[0]; // Get label
            var value = try std.fmt.parseInt(u8, pieces.items[1], 10); // Get focal length
            // Add to HashMap. If it conflicts, it overwrites the value while maintaining order
            try boxes[hash(label)].put(label, value);
        }
    }

    var total: usize = 0;
    for (0.., boxes) |i, box| {
        for (0.., box.values()) |slot, v| {
            // Calculate value based on the formula given by problem
            total += (i + 1) * (slot + 1) * v;
        }
    }

    try stdout.print("{d} \n", .{total});
}
