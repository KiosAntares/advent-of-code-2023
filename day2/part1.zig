const std = @import("std");

const Draw = struct {
    r: u32,
    g: u32,
    b: u32,
};

// Parses a game, and returns wether it was a valid game according to the rules
fn parseGame(str: []const u8) !bool {
    var draws = std.mem.split(u8, str, ";");
    while (draws.next()) |draw| {
        var d = try parseDraw(draw);
        if (!isGameValid(d)) {
            return false;
        }
    }
    return true;
}

fn isGameValid(game: Draw) bool {
    return game.r <= 12 and game.g <= 13 and game.b <= 14;
}

// Parse a draw line: 'x red, y green, z blue', all optional
fn parseDraw(str: []const u8) !Draw {
    var draw = Draw{
        .r = 0,
        .g = 0,
        .b = 0,
    };
    var cubesets = std.mem.split(u8, str, ",");
    while (cubesets.next()) |_cubeset| {
        // Sanitize any front or back spaces
        const cubeset = std.mem.trim(u8, _cubeset, " ");
        var tokens = std.mem.split(u8, cubeset, " ");
        const t = tokens.next() orelse "0"; // the number
        const number: u32 = try std.fmt.parseInt(u32, t, 10);
        const color = tokens.next() orelse ""; // the color
        if (std.mem.eql(u8, color, "red")) {
            draw.r = number;
        } else if (std.mem.eql(u8, color, "green")) {
            draw.g = number;
        } else {
            draw.b = number;
        }
    }
    return draw;
}

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();

    var total: u32 = 0;
    var buf: [1024]u8 = undefined;
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        if (line.len == 0) {
            break;
        }
        var gameS = std.mem.split(u8, line, ":");
        const gameInfo = gameS.next() orelse "";
        const gameID = try std.fmt.parseInt(u32, gameInfo[5..], 10);
        const game = gameS.next() orelse "";
        const valid = try parseGame(game);
        if (valid) {
            total += gameID;
        }
    }
    try stdout.print("Total: {d} \n", .{total});
}
