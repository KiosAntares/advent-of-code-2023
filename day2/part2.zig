const std = @import("std");

// Number of red, green, blue cubes
const Draw = struct {
    r: u32,
    g: u32,
    b: u32,
};

// Finds lowest number of cubes to get set that is valid
fn parseAndFindMinimumViableGame(str: []const u8) !Draw {
    var draws = std.mem.split(u8, str, ";");
    var minimum = Draw{
        .r = 0,
        .g = 0,
        .b = 0,
    };
    while (draws.next()) |draw| {
        const d = try parseDraw(draw);
        // If we needed more than we needed so far, update
        minimum.r = @max(d.r, minimum.r);
        minimum.g = @max(d.g, minimum.g);
        minimum.b = @max(d.b, minimum.b);
    }
    return minimum;
}

// Parse a draw line: 'x red, y green, z blue', all optional
fn parseDraw(str: []const u8) !Draw {
    var draw = Draw{
        .r = 0,
        .g = 0,
        .b = 0,
    };
    var cubesets = std.mem.split(u8, str, ",");
    while (cubesets.next()) |_cubeset| {
        const cubeset = std.mem.trim(u8, _cubeset, " ");
        var tokens = std.mem.split(u8, cubeset, " ");
        const t = tokens.next() orelse "0"; // the number
        const number: u32 = try std.fmt.parseInt(u32, t, 10);
        const color = tokens.next() orelse ""; // the color
        if (std.mem.eql(u8, color, "red")) {
            draw.r = number;
        } else if (std.mem.eql(u8, color, "green")) {
            draw.g = number;
        } else {
            draw.b = number;
        }
    }
    return draw;
}

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();

    var total: u32 = 0;
    var buf: [1024]u8 = undefined;
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        var gameS = std.mem.split(u8, line, ":");
        _ = gameS.next(); // We don't need game info anymore
        // const gameInfo = gameS.next() orelse "";
        // const gameID = try std.fmt.parseInt(u32, gameInfo[5..], 10);
        const game = gameS.next() orelse "";
        const min = try parseAndFindMinimumViableGame(game);
        const power = min.r * min.g * min.b;
        total += power;
    }
    try stdout.print("Total: {d} \n", .{total});
}
