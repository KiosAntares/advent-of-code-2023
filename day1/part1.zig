const std = @import("std");

// Finds digit characters from left and right. First valid ones are converted
// Edge case: there is only one digit. This is guaranteed not to happen
// by problem guarantees
fn getCalibration(str: []u8) u32 {
    var left: usize = 0;
    var right: usize = str.len - 1;

    while (!std.ascii.isDigit(str[left])) {
        left += 1;
    }
    while (!std.ascii.isDigit(str[right])) {
        right -= 1;
    }
    return (str[left] - '0') * 10 + (str[right] - '0');
}

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();

    var total_calibration: u32 = 0;
    var buf: [1024]u8 = undefined;
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        var cal = getCalibration(line);
        try stdout.print("{s} {d} \n", .{ line, cal });
        total_calibration += cal;
    }
    try stdout.print("Total: {d} \n", .{total_calibration});
}
