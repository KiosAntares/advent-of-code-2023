const std = @import("std");

// Finds first digit or spelled-out-digit
fn getNumber(str: []u8, dict: [9][]const u8) u32 {
    var index: usize = 0;

    // First we find the normal digit
    while (!std.ascii.isDigit(str[index])) {
        index += 1;
    }

    // Then we find the spelled out one
    // Whichever comes first is kept
    for (0..str.len) |i| {
        for (0.., dict) |j, word| {
            // Check if current splice (from i to end) starts with a valid word
            if (std.mem.startsWith(u8, str[i..], word) and i <= index) {
                index = i;
                // If so, the digit was the position of the word in the dictionary
                return @intCast(j + 1);
            }
        }
    }
    return str[index] - '0';
}

fn getCalibration(str: []u8) u32 {
    const words = [_][]const u8{ "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
    const words_reversed = [_][]const u8{ "eno", "owt", "eerht", "ruof", "evif", "xis", "neves", "thgie", "enin" };

    const first: u32 = getNumber(str, words);
    std.mem.reverse(u8, str);
    const second: u32 = getNumber(str, words_reversed);
    return first * 10 + second;
}

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();

    var total_calibration: u32 = 0;
    var buf: [1024]u8 = undefined;
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        var cal = getCalibration(line);
        try stdout.print("{s} {d} \n", .{ line, cal });
        total_calibration += cal;
    }
    try stdout.print("Total: {d} \n", .{total_calibration});
}
