const std = @import("std");

const Card = enum(u8) { two, three, four, five, six, seven, eight, nine, ten, jack, queen, king, ace };
const HandScore = enum(u8) { high, pair, twop, three, full, four, five };

const Hand = struct {
    cards: [5]Card,
    bid: u32,
    score: HandScore,
    _meta: [5]u8, // Keeping this for debug

    // Comparison function
    // Context, Left hand side, Right Hand side
    fn cmp(_: void, lhs: Hand, rhs: Hand) bool {
        const lhss = @intFromEnum(lhs.score);
        const rhss = @intFromEnum(rhs.score);
        // If the scoring is different, use scoring priority
        if (lhss < rhss) {
            return true;
        }
        if (lhss > rhss) {
            return false;
        }
        // if it's equal, iterate until we find different cards
        var i: usize = 0;
        while (lhs.cards[i] == rhs.cards[i]) {
            i += 1;
        }
        // and use card rank
        return @intFromEnum(lhs.cards[i]) < @intFromEnum(rhs.cards[i]);
    }
};

fn charToCard(char: u8) Card {
    return switch (char) {
        '2'...'9' => @enumFromInt(char - '2'),
        'T' => Card.ten,
        'J' => Card.jack,
        'Q' => Card.queen,
        'K' => Card.king,
        else => Card.ace,
    };
}

fn parseHand(str: []const u8) !Hand {
    var hand = Hand{
        .cards = undefined,
        .bid = undefined,
        .score = undefined,
        ._meta = undefined,
    };
    // I want to keep the original format for debugging so we do this
    std.mem.copyForwards(u8, &hand._meta, str[0..5]);
    // Convert the cards
    for (0..5) |i| {
        hand.cards[i] = charToCard(str[i]);
    }
    hand.bid = try std.fmt.parseInt(u32, str[6..], 10);
    // We score already, there's no more needed information
    hand.score = scoreHand(hand);
    return hand;
}

fn scoreHand(hand: Hand) HandScore {
    // Get occurrences count of every card
    var counts = [_]u32{0} ** 13;
    for (hand.cards) |card| {
        counts[@intFromEnum(card)] += 1;
    }

    // Get max count and amount of cards with a count of two
    var twos: u32 = 0;
    var max: u32 = 0;
    for (counts) |count| {
        max = @max(count, max);
        if (count == 2) {
            twos += 1;
        }
    }

    return switch (max) {
        5 => HandScore.five,
        4 => HandScore.four,
        3 => if (twos == 1) HandScore.full else HandScore.three,
        2 => if (twos == 2) HandScore.twop else HandScore.pair,
        else => HandScore.high,
    };
}

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
    var buf: [1024]u8 = undefined;

    var hands = std.ArrayList(Hand).init(std.heap.page_allocator);

    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        try hands.append(try parseHand(line));
    }

    // Sorting hand by rank
    std.mem.sort(Hand, hands.items, {}, Hand.cmp);

    var total: u32 = 0;
    for (1.., hands.items) |rank, hand| {
        var winning = hand.bid * @as(u32, @truncate(rank));
        total += winning;
        std.debug.print("{s} BID: {d:4}  SCORED: {s:5} adjusted {d} \n", .{ hand._meta, hand.bid, @tagName(hand.score), winning });
    }

    try stdout.print("{d} \n", .{total});
}
