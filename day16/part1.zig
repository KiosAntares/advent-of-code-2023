const std = @import("std");
const cm = @import("common.zig");

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
    var buf: [1024]u8 = undefined;

    var map = cm.Map.init(cm.all);
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        var row = cm.Row.init(cm.all);
        try row.appendSlice(line);
        try map.append(row);
    }

    // Prepare energisation map once and then reuse to reduce number of allocation
    // and memory usage
    const w = map.getLast().items.len;
    var energised = cm.EnerMap.init(cm.all);
    for (map.items) |row| {
        var irow = try cm.EnerRow.initCapacity(cm.all, w);
        for (row.items) |_| {
            try irow.append(cm.Dir.none);
        }
        try energised.append(irow);
    }

    var r = try cm.simulateGetCount(map, cm.Beam{ .col = 0, .row = 0, .ter = false, .dir = cm.Dir.right }, energised);

    try stdout.print("{d} \n", .{r});
}
