const std = @import("std");
const cm = @import("common.zig");

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
    var buf: [1024]u8 = undefined;

    var map = cm.Map.init(cm.all);
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        var row = cm.Row.init(cm.all);
        try row.appendSlice(line);
        try map.append(row);
    }

    const w = map.getLast().items.len;
    const h = map.items.len;
    var startingbeams = std.ArrayList(cm.Beam).init(cm.all);
    // Add all top and bottom row starting beams:
    for (0..w) |i| {
        try startingbeams.append(cm.Beam{
            .col = @intCast(i),
            .row = 0,
            .ter = false,
            .dir = cm.Dir.down,
        });
        try startingbeams.append(cm.Beam{
            .col = @intCast(i),
            .row = @intCast(h - 1),
            .ter = false,
            .dir = cm.Dir.up,
        });
    }
    // Add all left and right column starting beams
    for (0..h) |i| {
        try startingbeams.append(cm.Beam{
            .row = @intCast(i),
            .col = 0,
            .ter = false,
            .dir = cm.Dir.right,
        });
        try startingbeams.append(cm.Beam{
            .row = @intCast(i),
            .col = @intCast(w - 1),
            .ter = false,
            .dir = cm.Dir.left,
        });
    }
    // Prepare energisation map once and then reuse to reduce number of allocation
    // and memory usage
    var energised = cm.EnerMap.init(cm.all);
    for (map.items) |row| {
        var irow = try cm.EnerRow.initCapacity(cm.all, w);
        for (row.items) |_| {
            try irow.append(cm.Dir.none);
        }
        try energised.append(irow);
    }

    // Max count
    var m: usize = 0;
    for (startingbeams.items) |sbeam| {
        var r = try cm.simulateGetCount(map, sbeam, energised);
        m = @max(m, r);
    }

    try stdout.print("{d} \n", .{m});
}
