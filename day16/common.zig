const std = @import("std");

pub const Row = std.ArrayList(u8);
pub const Map = std.ArrayList(Row);
pub const all = std.heap.page_allocator;
pub const EnerRow = std.ArrayList(Dir);
pub const EnerMap = std.ArrayList(EnerRow);

pub const Dir = enum { up, right, down, left, none };

// A beam of light
pub const Beam = struct {
    row: i32,
    col: i32,
    dir: Dir,
    ter: bool, // is terminated (stop evaluation)
};

// All beams are terminated
pub fn allTerminated(beams: std.ArrayList(Beam)) bool {
    for (beams.items) |beam| {
        if (!beam.ter) {
            return false;
        }
    }
    return true;
}

// Count amount of energised cells in an energisation map
pub fn countEnergised(ener: std.ArrayList(std.ArrayList(Dir))) usize {
    var out: usize = 0;
    for (ener.items) |row| {
        for (row.items) |cell| {
            if (cell != Dir.none) {
                out += 1;
            }
        }
    }
    return out;
}

// Simulate beams and return amount of energised cells
pub fn simulateGetCount(map: Map, starting: Beam, energised: EnerMap) !usize {
    const w = map.getLast().items.len;
    const h = map.items.len;
    // All beams
    var beams = std.ArrayList(Beam).init(all);
    // Cells that have been energised, and in which direction it was passed
    // Construct it: none have been energised
    for (energised.items) |row| {
        for (row.items) |*cell| {
            cell.* = Dir.none;
        }
    }

    // Start from the starting beam
    try beams.append(starting);

    // until all the beams have stopped evaluation
    while (!allTerminated(beams)) {
        // Iterate backwards to deal with newly added beams
        var i = beams.items.len;
        while (i > 0) {
            i -= 1;
            // if the beam is terminated, skip it
            if (beams.items[i].ter) {
                continue;
            }
            // if the beam is out of bounds, it's terminated
            if (beams.items[i].col < 0 or beams.items[i].col >= w or beams.items[i].row < 0 or beams.items[i].row >= h) {
                beams.items[i].ter = true;
                continue;
            }
            const row: usize = @intCast(beams.items[i].row);
            const col: usize = @intCast(beams.items[i].col);
            const dir = beams.items[i].dir;

            // if a cell has already been energised by a beam in the same direction, we already evaluated this position
            // and we can stop this beam
            if (energised.items[row].items[col] == dir) {
                beams.items[i].ter = true;
                continue;
            }

            // We energised this cell
            energised.items[row].items[col] = dir;
            switch (map.items[row].items[col]) {
                // The various types of mirrors
                '\\' => {
                    switch (dir) {
                        Dir.up => {
                            beams.items[i].dir = Dir.left;
                            beams.items[i].col -= 1;
                        },
                        Dir.right => {
                            beams.items[i].dir = Dir.down;
                            beams.items[i].row += 1;
                        },
                        Dir.down => {
                            beams.items[i].dir = Dir.right;
                            beams.items[i].col += 1;
                        },
                        else => {
                            beams.items[i].dir = Dir.up;
                            beams.items[i].row -= 1;
                        },
                    }
                },
                '/' => {
                    switch (dir) {
                        Dir.up => {
                            beams.items[i].dir = Dir.right;
                            beams.items[i].col += 1;
                        },
                        Dir.right => {
                            beams.items[i].dir = Dir.up;
                            beams.items[i].row -= 1;
                        },
                        Dir.down => {
                            beams.items[i].dir = Dir.left;
                            beams.items[i].col -= 1;
                        },
                        else => {
                            beams.items[i].dir = Dir.down;
                            beams.items[i].row += 1;
                        },
                    }
                },
                '|' => {
                    switch (dir) {
                        Dir.up => {
                            beams.items[i].row -= 1;
                        },
                        Dir.down => {
                            beams.items[i].row += 1;
                        },
                        else => {
                            beams.items[i].dir = Dir.down;
                            beams.items[i].row += 1;
                            try beams.append(Beam{
                                .col = beams.items[i].col,
                                .row = beams.items[i].row - 1,
                                .dir = Dir.up,
                                .ter = false,
                            });
                        },
                    }
                },
                '-' => {
                    switch (dir) {
                        Dir.left => {
                            beams.items[i].col -= 1;
                        },
                        Dir.right => {
                            beams.items[i].col += 1;
                        },
                        else => {
                            beams.items[i].dir = Dir.left;
                            beams.items[i].col -= 1;
                            try beams.append(Beam{
                                .col = beams.items[i].col + 1,
                                .row = beams.items[i].row,
                                .dir = Dir.right,
                                .ter = false,
                            });
                        },
                    }
                },

                else => {
                    switch (beams.items[i].dir) {
                        Dir.up => {
                            beams.items[i].row -= 1;
                        },
                        Dir.right => {
                            beams.items[i].col += 1;
                        },
                        Dir.down => {
                            beams.items[i].row += 1;
                        },
                        else => {
                            beams.items[i].col -= 1;
                        },
                    }
                },
            }
        }
    }

    // Deallocate
    var k = countEnergised(energised);
    beams.deinit();
    return k;
}
