# advent-of-code-2023

Advent of Code challenges from 2023. 
Implemented in Zig initially and, eventually, possibly in other languages as well.

## Usage
Decent build system to come. You can always run it by using `zig run <filename>`.
