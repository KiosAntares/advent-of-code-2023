const std = @import("std");

const stdout = std.io.getStdOut().writer();

const Coord = struct {
    col: i32,
    row: i32,
};

const Part = struct {
    value: i32,
    len: i32,
    position: Coord,
};

const Gear = struct {
    position: Coord,
    value: i32,
    count: i32,
};

// Finds all "parts" in the grid (consecutive strings of digits)
fn findParts(grid: [][200]u8) !std.ArrayList(Part) {
    var list = std.ArrayList(Part).init(std.heap.page_allocator);
    for (0.., grid) |i, row| {
        var part = Part{
            .value = 0,
            .len = 0,
            .position = undefined,
        };
        for (0.., row) |j, item| {
            if (std.ascii.isDigit(item)) {
                if (part.value == 0) {
                    part.position = Coord{
                        .col = @intCast(j),
                        .row = @intCast(i),
                    };
                }
                part.value = part.value * 10 + (item - '0');
                part.len += 1;
            } else {
                if (part.value != 0) {
                    try list.append(part);
                }
                part.value = 0;
                part.len = 0;
            }
        }
        if (part.value != 0) {
            try list.append(part);
        }
    }
    return list;
}

// Explores all parts and finds gears in adjacent cells .
// When a gear is found, it is checked against a map of existing gears,
// and added if not stored. Existing gears are updated to indicate that
// they affect another part.
fn exploreAll(parts: []Part, grid: [][200]u8) !std.AutoHashMap(Coord, Gear) {
    var gears = std.AutoHashMap(Coord, Gear).init(std.heap.page_allocator);
    for (parts) |part| {
        // We calculate bounds for the row search, the row before and after the current
        // By using min and max we can constrain boundaries to the grid
        var row_lowbound = @max(0, part.position.row - 1);
        var row_highbound = @min(grid.len, @as(usize, @intCast(part.position.row + 1))) + 1;
        for (row_lowbound..row_highbound) |i| {
            // Same for columns, but here the range is based on the length of the part
            var col_lowbound = @max(0, part.position.col - 1);
            var col_highbound = @min(grid[i].len, @as(usize, @intCast(part.position.col + part.len))) + 1;
            for (col_lowbound..col_highbound) |j| {
                // We can ignore non-gears, which includes the part itself and other parts
                if (grid[i][j] == '*') {
                    var coord = Coord{ .row = @intCast(i), .col = @intCast(j) };
                    if (gears.getPtr(coord)) |gearp| {
                        gearp.*.value *= part.value;
                        gearp.*.count += 1;
                    } else {
                        var newgear = Gear{
                            .position = coord,
                            .value = part.value,
                            .count = 1,
                        };
                        try gears.put(coord, newgear);
                    }
                }
            }
        }
    }
    return gears;
}

pub fn main() !void {
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();

    var grid: [200][200]u8 = [_][200]u8{.{0} ** 200} ** 200;

    var buf: [1024]u8 = undefined;
    var lc: usize = 0;
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        std.mem.copyForwards(u8, &grid[lc], line);
        lc += 1;
    }

    var total: i32 = 0;
    var parts = try findParts(&grid);
    var gears = try exploreAll(parts.items, &grid);

    var i = gears.valueIterator();
    while (i.next()) |gear| {
        std.debug.print("Gear at ({d},{d}): {d} parts for value = {d}\n", .{ gear.position.row, gear.position.col, gear.count, gear.value });
        if (gear.count == 2) { // Gears should only mesh two parts
            total += gear.value;
        }
    }

    try stdout.print("Total: {d} \n", .{total});
}
