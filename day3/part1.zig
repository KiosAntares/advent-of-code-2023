const std = @import("std");

const stdout = std.io.getStdOut().writer();

const Coord = struct {
    col: i32,
    row: i32,
};

const Part = struct {
    value: i32,
    len: i32,
    position: Coord,
};

// Finds all "parts" in the grid (consecutive strings of digits)
fn findParts(grid: [][200]u8) !std.ArrayList(Part) {
    var list = std.ArrayList(Part).init(std.heap.page_allocator);
    for (0.., grid) |i, row| {
        var part = Part{
            .value = 0,
            .len = 0,
            .position = undefined,
        };
        for (0.., row) |j, item| {
            if (std.ascii.isDigit(item)) {
                if (part.value == 0) {
                    part.position = Coord{
                        .col = @intCast(j),
                        .row = @intCast(i),
                    };
                }
                part.value = part.value * 10 + (item - '0');
                part.len += 1;
            } else {
                if (part.value != 0) {
                    try list.append(part);
                }
                part.value = 0;
                part.len = 0;
            }
        }
        if (part.value != 0) {
            try list.append(part);
        }
    }
    return list;
}

// Exlores the cells adjecent to the part cells, including diagonals
fn getNeighbors(part: Part, grid: [][200]u8) !std.ArrayList(u8) {
    var list = std.ArrayList(u8).init(std.heap.page_allocator);
    // We calculate bounds for the row search, the row before and after the current
    // By using min and max we can constrain boundaries to the grid
    var row_lowbound = @max(0, part.position.row - 1);
    var row_highbound = @min(grid.len, @as(usize, @intCast(part.position.row + 1))) + 1;
    for (row_lowbound..row_highbound) |i| {
        // Same for columns, but here the range is based on the length of the part
        var col_lowbound = @max(0, part.position.col - 1);
        var col_highbound = @min(grid[i].len, @as(usize, @intCast(part.position.col + part.len))) + 1;
        for (col_lowbound..col_highbound) |j| {
            // Here we safeguard against reading any non valid characters,
            // e.g. out of bound into the initialized buffer
            if (std.ascii.isPrint(grid[i][j])) {
                try list.append(grid[i][j]);
            }
        }
    }
    return list;
}

// A valid part must contain a symbol in its neighbours
fn shouldBeCounted(neighs: []u8) bool {
    for (neighs) |n| {
        // We ignore other parts. Also, this allows us to count
        // the part itself as its neighbour as it won't affect the count
        if (!std.ascii.isDigit(n) and n != '.') {
            return true;
        }
    }
    return false;
}

pub fn main() !void {
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();

    var grid: [200][200]u8 = [_][200]u8{.{0} ** 200} ** 200;

    var buf: [1024]u8 = undefined;
    var lc: usize = 0;
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        std.mem.copyForwards(u8, &grid[lc], line);
        lc += 1;
    }

    var total: i32 = 0;
    var parts = try findParts(&grid);
    for (parts.items) |part| {
        std.debug.print("Part {d} ({d},{d})\n", .{ part.value, part.position.row, part.position.col });
        var n = try getNeighbors(part, &grid);
        var count = shouldBeCounted(n.items);
        std.debug.print("Neighs: {s}, should count: {} \n", .{ n.items, count });
        if (count) {
            total += part.value;
        }
    }

    try stdout.print("Total: {d} \n", .{total});
}
