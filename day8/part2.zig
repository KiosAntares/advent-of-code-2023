const std = @import("std");
const cm = @import("common.zig");

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
    var buf: [1024]u8 = undefined;

    var pattern = std.ArrayList(u8).init(std.heap.page_allocator);
    var map: [cm.ZZZ + 1]cm.Node = undefined;

    if (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        try pattern.appendSlice(line);
    }

    var starting_nodes = std.ArrayList(u32).init(std.heap.page_allocator);

    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        if (line.len == 0) continue;
        const node = cm.parseNode(line);
        const id = cm.hasher(line[0..3]);
        map[id] = node;
        if (id % 100 == cm.A) {
            try starting_nodes.append(id);
        }
    }

    var counts = std.ArrayList(usize).init(std.heap.page_allocator);
    for (starting_nodes.items) |*node| {
        var steps: usize = 0;
        while (!cm.endingNode(node.*)) {
            const dir = pattern.items[steps % pattern.items.len];
            const mapped = map[node.*];
            node.* = switch (dir) {
                'L' => mapped.lh,
                'R' => mapped.rh,
                else => 252525,
            };
            steps += 1;
        }
        try counts.append(steps);
    }

    const result = cm.lcm(counts.items);
    try stdout.print("{d} \n", .{result});
}
