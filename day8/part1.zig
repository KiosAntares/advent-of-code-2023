const std = @import("std");
const cm = @import("common.zig");

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
    var buf: [1024]u8 = undefined;

    var pattern = std.ArrayList(u8).init(std.heap.page_allocator);
    var map: [cm.ZZZ + 1]cm.Node = undefined;

    if (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        try pattern.appendSlice(line);
    }

    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        if (line.len == 0) continue;
        const node = cm.parseNode(line);
        const id = cm.hasher(line[0..3]);
        map[id] = node;
    }

    var current: u32 = cm.AAA;
    var steps: usize = 0;
    while (current != cm.ZZZ) {
        const dir = pattern.items[steps % pattern.items.len];
        const node = map[current];
        current = switch (dir) {
            'L' => node.lh,
            'R' => node.rh,
            else => cm.ZZZ,
        };
        steps += 1;
    }
    try stdout.print("{d} \n", .{steps});
}
