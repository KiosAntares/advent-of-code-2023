const std = @import("std");

pub const A: u32 = 17;
pub const Z: u32 = 42;
pub const AAA: u32 = 171717;
pub const ZZZ: u32 = 424242;

pub const Node = struct {
    lh: u32,
    rh: u32,
};

pub fn rotate(pattern: []const u8, reset: bool) u8 {
    const state = struct {
        var counter: usize = 0;
    };
    if (reset) {
        state.counter = 0;
        return 0;
    }
    state.counter += 1;
    return pattern[(state.counter - 1) % pattern.len];
}

pub fn hasher(str: []const u8) u32 {
    return @as(u32, (str[0] - '0')) * 10000 + @as(u32, (str[1] - '0')) * 100 + (str[2] - '0');
}

pub fn endingNode(node: u32) bool {
    return (node % 100) == Z;
}

pub fn parseNode(str: []u8) Node {
    var n = Node{
        .lh = hasher(str[7..10]),
        .rh = hasher(str[12..15]),
    };
    return n;
}

pub fn lcm(items: []usize) usize {
    var l = items[0];
    for (items[1..]) |item| {
        l = (l * item) / std.math.gcd(l, item);
    }
    return l;
}
