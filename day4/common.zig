const std = @import("std");

pub const Card = struct {
    winning: [10]i32,
    given: [25]i32,
    copies: u32,
};

pub fn parseCard(str: []const u8) !Card {
    var segments = std.mem.split(u8, str, "|");
    var winning = segments.next() orelse "";
    var given = segments.next() orelse "";
    var winnings = std.mem.split(u8, std.mem.trim(u8, winning, " "), " ");
    var card = Card{
        .winning = [_]i32{0} ** 10,
        .given = [_]i32{0} ** 25,
        .copies = 1,
    };
    var i: usize = 0;
    while (winnings.next()) |win| {
        if (win.len == 0) {
            continue;
        }
        card.winning[i] = try std.fmt.parseInt(i32, win, 10);
        i += 1;
    }
    i = 0;
    var givens = std.mem.split(u8, std.mem.trim(u8, given, " "), " ");
    while (givens.next()) |giv| {
        if (giv.len == 0) {
            continue;
        }
        card.given[i] = try std.fmt.parseInt(i32, giv, 10);
        i += 1;
    }
    return card;
}

pub fn parseLine(str: []const u8) !Card {
    var segments = std.mem.split(u8, str, ":");
    _ = segments.next() orelse ""; // unused, but kept
    var numbers = segments.next() orelse "";
    var card = try parseCard(numbers);
    return card;
}

pub fn countWins(card: Card) u32 {
    var count: u32 = 0;
    var i: usize = 0;
    while (i < card.winning.len and card.winning[i] != 0) : (i += 1) {
        var j: usize = 0;
        while (j < card.given.len and card.given[j] != 0) : (j += 1) {
            if (card.winning[i] == card.given[j]) {
                count += 1;
            }
        }
    }
    return count;
}

fn scoring2Pow(count: u32) u32 {
    if (count == 0) {
        return 0;
    } else return @as(u32, 1) << @truncate(count - 1);
}

pub fn score(card: Card) u32 {
    return scoring2Pow(countWins(card));
}
