const std = @import("std");
const cm = @import("common.zig");

fn dbgCards(cards: std.ArrayList(cm.Card)) void {
    for (0.., cards.items) |i, c| {
        std.debug.print("Card {}: {} copies\n", .{ i + 1, c.copies });
    }
}

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("test", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();

    var total: u32 = 0;
    var buf: [1024]u8 = undefined;
    var cards = std.ArrayList(cm.Card).init(std.heap.page_allocator);
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        if (line.len == 0) {
            break;
        }
        var card: cm.Card = try cm.parseLine(line);
        try cards.append(card);
    }

    const l = cards.items.len;
    std.debug.print("l: {}\n", .{l});
    for (0.., cards.items[0 .. l - 1]) |i, card| {
        var count = cm.countWins(card);
        const lbound: usize = @min(l - 1, i + 1);
        const hbound: usize = @min(l - 1, i + count + 1);
        std.debug.print("ANALYZING CARD _{} Count: {}, bounds {} {} \n", .{ i + 1, count, lbound, hbound });
        for (cards.items[lbound..hbound]) |*c| {
            c.*.copies += card.copies;
        }
        dbgCards(cards);
    }

    for (cards.items) |card| {
        total += card.copies;
    }

    try stdout.print("Total: {d} \n", .{total});
}
