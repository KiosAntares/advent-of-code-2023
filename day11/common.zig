const std = @import("std");

pub const Row = std.ArrayList(u8);
pub const Map = std.ArrayList(Row);
pub const GalaxyCollection = std.ArrayList(Galaxy);
pub const all = std.heap.page_allocator;

pub const Galaxy = struct {
    row: usize,
    col: usize,
};

// Manhattan distance, without using ABS to deal with unsigned-ness
fn distance(g1: Galaxy, g2: Galaxy) u64 {
    return (@max(g1.row, g2.row) - @min(g1.row, g2.row)) + (@max(g1.col, g2.col) - @min(g1.col, g2.col));
}

// Find galaxies in the map and return them, ORDERED top to bottom left to right
pub fn findGalaxies(map: Map) !GalaxyCollection {
    var galaxies = GalaxyCollection.init(all);
    for (0.., map.items) |rowi, row| {
        for (0.., row.items) |coli, cell| {
            if (cell == '#') {
                try galaxies.append(Galaxy{
                    .row = rowi,
                    .col = coli,
                });
            }
        }
    }
    return galaxies;
}

// Scale the coordinates of the galaxies based on the map conformation.
// columns or rows of empty space (no galaxies) count as FACTOR + 1 rows/columns
pub fn scaleMap(map: Map, galaxies: GalaxyCollection, factor: usize) !GalaxyCollection {
    // Use a work collection separate of the source one.
    // This way we can still use the original position in the map of the galaxy for the check
    // As the adjustments will influence the positions
    var adj = try galaxies.clone();
    // First, scale the rows
    for (0.., map.items) |i, row| {
        // Find out if the row is empty
        var empty = true;
        for (row.items) |cell| {
            if (cell == '#') {
                empty = false;
                break;
            }
        }

        // If it is,
        if (empty) {
            for (0.., galaxies.items) |gi, g| {
                // Scale galaxies that are placed after this row
                if (g.row > i) {
                    adj.items[gi].row += factor;
                }
            }
        }
    }

    // Then the columns
    const cols = map.getLast().items.len;
    for (0..cols) |col| {
        var empty = true;
        for (map.items) |row| {
            if (row.items[col] == '#') {
                empty = false;
                break;
            }
        }

        if (empty) {
            for (0.., galaxies.items) |gi, g| {
                if (g.col > col) {
                    adj.items[gi].col += factor;
                }
            }
        }
    }
    return adj;
}

// Calculate sum of the distance between every pair of galaxies
pub fn calculateDistancies(galaxies: GalaxyCollection) u64 {
    var total: u64 = 0;
    const l = galaxies.items.len;
    // We can ignore the last galaxy: all its pairs will already have been calculated
    for (0.., galaxies.items[0 .. l - 1]) |i, g| {
        // Only check galaxies after this one, the ones before already considered this
        // galaxy in their turn
        for (galaxies.items[i + 1 .. l]) |k| {
            // Use the Manhattan distance, as it is a grid
            const d = distance(g, k);
            total += d;
        }
    }
    return total;
}
