const std = @import("std");
const cm = @import("common.zig");

const FACTOR = 2;

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
    var buf: [1024]u8 = undefined;

    var map = cm.Map.init(cm.all);
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        var row = cm.Row.init(cm.all);
        try row.appendSlice(line);
        try map.append(row);
    }

    // Find galaxies in the map and return them
    var galaxies = try cm.findGalaxies(map);
    // subtract one because factor is multiplicative, we have to ignore the
    // existing one
    galaxies = try cm.scaleMap(map, galaxies, FACTOR - 1);
    // Calculate distance between every pair of galaxies and sum them
    const total = cm.calculateDistancies(galaxies);
    try stdout.print("{d} \n", .{total});
}
