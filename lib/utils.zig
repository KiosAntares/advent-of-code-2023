const std = @import("std");

// Given a 'sep' separated list of numbers, sanify and return it
pub fn parseSeparatedList(comptime T: type, str: []const u8, sep: []const u8) !std.ArrayList(T) {
    var s = std.mem.split(u8, std.mem.trim(u8, str, " "), sep);
    var a = std.ArrayList(T).init(std.heap.page_allocator);
    while (s.next()) |num| {
        if (num.len == 0) continue;
        try a.append(try std.fmt.parseInt(T, std.mem.trim(u8, num, " "), 10));
    }
    return a;
}

// Split on 'sep' and return arraylist of tokens
pub fn splitOn(str: []const u8, sep: []const u8) !std.ArrayList([]const u8) {
    var s = std.mem.split(u8, str, sep);
    var a = std.ArrayList([]const u8).init(std.heap.page_allocator);
    while (s.next()) |token| {
        if (token.len == 0) continue;
        try a.append(token);
    }
    return a;
}
