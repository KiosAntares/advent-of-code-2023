const std = @import("std");
const cm = @import("common.zig");

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
    var buf: [1024]u8 = undefined;

    var seeds: std.ArrayList(usize) = undefined;
    var maps = [_]cm.Map{
        cm.newMap(),
        cm.newMap(),
        cm.newMap(),
        cm.newMap(),
        cm.newMap(),
        cm.newMap(),
        cm.newMap(),
    };

    var mode: usize = 0; // 0: seeds, 1+: maps
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        // Empty lines divide blocks
        if (line.len == 0) {
            mode += 1;
            continue;
        }
        if (!std.ascii.isDigit(line[0]) and !std.mem.startsWith(u8, line, "seeds")) {
            // header lines
            // we ignore the first line (seeds line) as it doesnt follow the same format
            // as the others.
            continue;
        }

        if (mode == 0) {
            seeds = try cm.parseNumberList(line[6..]);
        } else try cm.parseMapLine(line, &maps[mode - 1]);
    }

    var lowest: usize = std.math.maxInt(usize);
    for (seeds.items) |seed| {
        var m = seed;
        for (maps) |map| {
            m = cm.getOrAssume(map, m);
        }
        std.debug.print("Seed {d}: soil {d}\n", .{ seed, m });
        if (m <= lowest) {
            lowest = m;
        }
    }

    try stdout.print("{d} \n", .{lowest});
}
