const std = @import("std");
const cm = @import("common.zig");

// Parses seeds pairwise (start length)
fn parseSeedsList(str: []const u8) !std.ArrayList(cm.Range) {
    var a = std.ArrayList(cm.Range).init(std.heap.page_allocator);
    const r = (try cm.parseNumberList(str)).items;
    var i: usize = 0;
    while (i < r.len) : (i += 2) {
        try a.append(cm.Range{
            .l = r[i],
            .h = r[i] + r[i + 1],
            .d = undefined,
        });
    }
    return a;
}

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const file = try std.fs.cwd().openFile("test", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
    var buf: [1024]u8 = undefined;

    var seeds: std.ArrayList(cm.Range) = undefined;
    var maps = [_]cm.Map{
        cm.newMap(),
        cm.newMap(),
        cm.newMap(),
        cm.newMap(),
        cm.newMap(),
        cm.newMap(),
        cm.newMap(),
    };

    var mode: usize = 0; // 0: seeds, 1+: maps
    while (try in_stream.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        // Empty lines divide blocks
        if (line.len == 0) {
            mode += 1;
            continue;
        }
        if (!std.ascii.isDigit(line[0]) and !std.mem.startsWith(u8, line, "seeds")) {
            // header lines
            // we ignore the first line (seeds line) as it doesnt follow the same format
            // as the others.
            continue;
        }

        if (mode == 0) {
            seeds = try parseSeedsList(line[6..]);
            continue;
        }

        try cm.parseMapLine(line, &maps[mode - 1]);
    }

    var lowest: usize = std.math.maxInt(usize);
    for (seeds.items) |seedR| {
        std.debug.print("Range {d}-{d}\n", .{ seedR.l, seedR.h });
        for (seedR.l..seedR.h) |seed| {
            var m = seed;
            for (maps) |map| {
                m = cm.getOrAssume(map, m);
            }
            lowest = @min(m, lowest);
        }
    }

    try stdout.print("{d} \n", .{lowest});
}
