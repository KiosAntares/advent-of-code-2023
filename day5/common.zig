const std = @import("std");
const util = @import("lib/utils.zig");

pub const Range = struct {
    l: usize,
    h: usize,
    d: usize,
};

pub const Map = struct {
    ranges: std.ArrayList(Range),
};

// allocates a map
pub fn newMap() Map {
    return Map{ .ranges = std.ArrayList(Range).init(std.heap.page_allocator) };
}

// Given a space-separated list of numbers, returns an arraylist of it
pub fn parseNumberList(str: []const u8) !std.ArrayList(usize) {
    return util.parseSeparatedList(usize, str, " ");
}

// Given a "destination source lenght" line, parses the range and adds it to the map
pub fn parseMapLine(str: []const u8, map: *Map) !void {
    const nums = try parseNumberList(str);
    const des = nums.items[0];
    const src = nums.items[1];
    const len = nums.items[2];

    try map.ranges.append(Range{
        .l = src,
        .h = src + len,
        .d = des,
    });
}

// Returns the mapped position in the range if the key is in a range.
// If not, the mapped position is the same as the key.
pub fn getOrAssume(map: Map, key: usize) usize {
    for (map.ranges.items) |range| {
        if (key >= range.l and key <= range.h) {
            return range.d + key - range.l;
        }
    }
    return key;
}
